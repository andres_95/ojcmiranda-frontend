import Vue from 'vue';
import VueRouter from 'vue-router';

// Layouts
import AuthLayout from '@/components/layout/AuthLayout.vue';

// Main View
import MainView from '@/views/MainView.vue';

// Auth Views
import LoginView from '@/views/Auth/LoginView.vue';
import RegisterView from '@/views/Auth/RegisterView.vue';
import ForgetPasswordView from '@/views/Auth/ForgetPasswordView.vue';
import NewPasswordView from '@/views/Auth/NewPasswordView.vue';
import InstitucionRegister from '@/views/InstitucionForms/InstitucionRegister.vue';
import EquipoRegister from '@/views/InstitucionForms/EquipoRegister.vue';
import AdminRegister from '@/views/AdminForms/AdminRegister.vue';
import JornadaRegister from '@/views/AdminForms/JornadaRegister.vue';

// Home Views
import HomeView from '@/views/Home/HomeView.vue';
import HomeAdmin from '@/views/Home/HomeAdmin.vue';
import DetalleInstituto from "@/views/Institucion/DetalleInstituto";
import DetailsInstituteById from '@/views/Institucion/DetailsInstituteById';
import Estatus from "@/views/Institucion/Estatus";
import InstitutoList from "@/views/Institucion/InstitutoList";
import TeamList from '@/views/Equipo/TeamList';
import DetailsTeam from '@/views/Equipo/DetailsTeam';
import JornadaList from "@/views/Jornada/JornadaList";
import PreRegistroList from '@/views/PreRegistro/PreRegistroList';
import PreliminarForm from "@/views/AdminForms/PreliminarForm";
import PruebaForm from "@/views/AdminForms/PruebaForm";
import ResultadoPruebaPreliminar from "@/views/Resultados/ResultadoPruebaPreliminar";
import ResultadoFinales from "@/views/Resultados/ResultadoFinales";
import ModifyPreliminar from '@/views/AdminForms/ModifyPreliminar';
import ModifyPrueba from '@/views/AdminForms/ModifyPrueba';

Vue.use(VueRouter);

const routes = [
  {
    path: '/auth',
    component: AuthLayout,
    redirect: '/auth/login',
    children: [
      {
        path: 'login',
        name: 'Login',
        meta: {
          guest: true,
          auth: false
        },
        component: LoginView
      },
      {
        path: 'register',
        name: 'Register',
        meta: {
          guest: true,
          auth: false
        },
        component: RegisterView
      },
      {
        path: 'forget-password',
        name: 'ForgetPassword',
        meta: {
          guest: true,
          auth: false
        },
        component: ForgetPasswordView
      },
      {
        path: 'new-password',
        name: 'NewPassword',
        props: true,
        meta: {
          guest: true,
          auth: false
        },
        component: NewPasswordView
      },  
    ]
  },
  {
    path: '/',
    component: MainView,
    children: [
      {
        path: '',
        name: 'HomeView',
        meta: {
          guest: false,
          auth: true
        },
        component: HomeView
      },
      {
        path: 'admin',
        name: 'admin',
        meta: {
          guest: false,
          auth: true
        },
        component: HomeAdmin
      },
      {
        path: 'equipo',
        name: 'equipo',
        meta: {
          guest: false,
          auth: true
        },
        component: EquipoRegister
      },
      {
        path: 'register',
        name: 'register',
        meta: {
          guest: false,
          auth: true
        },
        component: InstitucionRegister
      },
      {
        path: 'jornadaregister',
        name: 'jornadaregister',
        meta: {
          guest: false,
          auth: true
        },
        component: JornadaRegister
      },
      {
        path: 'adminregister',
        name: 'adminregister',
        meta: {
          guest: false,
          auth: true
        },
        component: AdminRegister
      },
      {
        path: 'institutodetalle',
        name: 'institutodetalle',
        meta: {
          guest: false,
          auth: true
        },
        component: DetalleInstituto
      },
      {
        path: 'institutodetalle/:id',
        name: 'detalleInstitutoPorId',
        meta: {
          guest: false,
          auth: true
        },
        component: DetailsInstituteById
      },
      {
        path: 'institutostatus',
        name: 'institutostatus',
        meta: {
          guest: false,
          auth: true
        },
        component: Estatus
      },
      {
        path: 'instituto',
        name: 'institutolist',
        meta: {
          guest: false,
          auth: true
        },
        component: InstitutoList
      },
      {
        path: 'listado-equipo',
        name: 'teamlist',
        meta: {
          guest: false,
          auth: true
        },
        component: TeamList
      },
      {
        path: 'detalles-equipo',
        name: 'team-details',
        props: true,
        meta: {
          guest: false,
          auth: true
        },
        component: DetailsTeam
      },
      {
        path: 'jornada',
        name: 'jornadalist',
        meta: {
          guest: false,
          auth: true
        },
        component: JornadaList
      },
      {
        path: 'preregistro',
        name: 'preregistro',
        meta: {
          guest: false,
          auth: true
        },
        component: PreRegistroList
      },
      {
        path: 'preliminarform',
        name: 'preliminarform',
        meta: {
          guest: false,
          auth: true
        },
        component: PreliminarForm
      },
      {
        path: 'pruebaform',
        name: 'pruebaform',
        meta: {
          guest: false,
          auth: true
        },
        component: PruebaForm
      },
      {
        path: 'modificar-prueba',
        name: 'modificarPrueba',
        props: true,
        meta: {
          guest: false,
          auth: true
        },
        component: ModifyPrueba
      },
      {
        path: 'resultadopreliminar',
        name: 'resultadoPreliminar',
        meta: {
          guest: false,
          auth: true
        },
        component: ResultadoPruebaPreliminar
      },
      {
        path: 'modificar-preliminar',
        name: 'modificarPreliminar',
        props: true,
        meta: {
          guest: false,
          auth: true
        },
        component: ModifyPreliminar
      },
      {
        path: 'resultadojornada',
        name: 'resultadojornada',
        meta: {
          guest: false,
          auth: true
        },
        component: ResultadoFinales
      }
    ]
  },


];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const isGuest   = to.matched.some(record => record.meta.guest);
  const checkAuth = to.matched.some(record => record.meta.auth);
  const global    = to.matched.some(record => record.meta.global);
  const tokenAuth = localStorage.getItem('access_token')

  if (global) {
    return next();
  }

  if (isGuest && tokenAuth) {
    return next({
      name: 'HomeView',
      force: true,
      replace: true
    })
  }

  if (checkAuth && !tokenAuth) {
    return next({
      name: 'Login',
      force: true,
      replace: true
    })
  }

  next();
})

export default router;