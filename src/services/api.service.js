import axios from 'axios';
import {TokenService} from '@/services/token.service';

// import {store} from '@/store';

class ApiService {
    _requestInterceptor = 0
    _401interceptor = 0

    http

    constructor() {
        this.http = axios.create({
            baseURL: process.env.API_URL || 'http://localhost:8000/api'
        })
    }

    setHeader() {
        this.http.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${TokenService.getToken()}`;
        this.http.defaults.headers.common[
            'Accept'
            ] = `application/json`;
    }

    getHeader() {
        return this.http.defaults.headers.common
    }

    removeHeader() {
        this.http.defaults.headers.common = {};
    }

    get(resource) {
        return this.http.get(resource);
    }

    post(resource, data) {
        return this.http.post(resource, data);
    }

    postCustom(resource, data, config) {
        return this.http.post(resource, data, config);
    }

    put(resource, data) {
        return this.http.put(resource, data);
    }

    patch(resource, data) {
        return this.http.patch(resource, data);
    }

    delete(resource) {
        return this.http.delete(resource);
    }

    customRequest(data) {
        return this.http(data);
    }
}

const apiService = new ApiService()

export default apiService;

