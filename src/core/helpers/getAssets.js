export const getIllustrationsPath = (illustrationName) => {
    return require('@/assets/' + illustrationName)
};