import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import './registerServiceWorker';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAWN from "vue-awesome-notifications"
import 'vue-awesome-notifications/dist/styles/style.css';

Vue.config.productionTip = false;

const api = axios.create({
  baseURL: process.env.VUE_APP_API_URL
})

Vue.use(VueAxios, api)

Vue.use(VueAWN, {
  maxNotifications: 2,
  durations: {
    global: 4000,
    alert: 2000
  }
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
