import Vue from "vue";

const accountStore = {
    namespaced: true,
    state: {
        user: {
            apellidos: null,
            email: '',
            nombres: '',
            role: '',
            username: ''
        },
        unidad_educativa: null
    },
    actions: {
        getAccount: ({state}) => {
            return new Promise((resolve, reject) => {
                Vue.axios.get('/account')
                    .then(value => {
                        state.user = value.data
                        state.unidad_educativa = value.data.unidad_educativa
                        resolve(value)
                    })
                    .catch(reason => {
                        reject(reason)
                    })
            })
        }
    },
    mutations: {
        updateUnidadEducativa(state, payload) {
            state.unidad_educativa = payload;
        }
    }

}

export default accountStore