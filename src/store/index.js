import Vue from 'vue'
import Vuex from 'vuex'
import authStore from "@/store/authStore";
import accountStore from "@/store/accountStore";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    authStore,
    accountStore:accountStore
  }
})
