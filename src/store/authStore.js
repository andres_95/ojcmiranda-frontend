import {TokenService} from "@/services/token.service";
import Vue from "vue";

const authStore = {
    namespaced: true,
    state: {
        token: '',
    },
    actions: {
        login: ({state}, payload) => {
            return new Promise((resolve, reject) => {
                Vue.axios.post('/auth/login', payload)
                    .then(value => {
                        TokenService.saveToken(value.data.token)
                        Vue.axios.defaults.headers.common[
                            'Authorization'
                            ] = `Bearer ${TokenService.getToken()}`;
                        Vue.axios.defaults.headers.common[
                            'Accept'
                            ] = `application/json`;
                        state.token = value.data.token
                        resolve(value)
                    })
                    .catch(reason => {
                        reject(reason)
                    })
            })
        }
    }
}

export default authStore;